package com.qi.student.student

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.qi.student.ClassDetails.InsertClassData
import com.qi.student.R

class StudentActivity : AppCompatActivity() {

    private var regStudent: Button? = null
    private var recyclerviewstudent: RecyclerView? = null

    var studentmodel = StudentModel()

    companion object {

        lateinit var studentdata: InsertStudentDetail

        lateinit var insertclassdata : InsertClassData
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recyclerview_studentname)

        var action = supportActionBar
        action?.setDisplayHomeAsUpEnabled(true)
        action?.setDisplayShowHomeEnabled(true)

        regStudent = findViewById(R.id.studentaddbutton)

        recyclerviewstudent = findViewById(R.id.studentrecycleviewer)

        //var studentdetails = ArrayList<StudentModel>()

        //studentdetails.add(StudentModel("1","Ritam de","barrackpore"))
        //studentdetails.add(StudentModel("2","Shubham roy","agarpara"))

        //var studentAdapter = StudentAdapter(studentdetails)
        //recyclerviewstudent?.adapter = studentAdapter

        studentdata = InsertStudentDetail(this)

        insertclassdata = InsertClassData(this)

        regStudent?.setOnClickListener {
            startActivityForResult(Intent(this, AddStudentActivity::class.java),1)
        }

        viewstudentlist()

        var classlist =  insertclassdata.fetchData(this)

        var spin = findViewById(R.id.classspinner) as Spinner

        var arrayadapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,classlist)

        arrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spin.adapter = arrayadapter

        spin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    viewclasslist(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>) {

                // write code to perform some action
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            viewstudentlist()
        }
    }

    fun viewstudentlist() {

        var studentlist = studentdata.fetchStudentDetails(this)
        var studentadapter = StudentAdapter(studentlist, object : StudentAdapter.StudentSelectedCallback {

                override fun onStudentClicked(studentModel: StudentModel) {

                    startActivityForResult(Intent(this@StudentActivity, UpdateDetail::class.java).putExtra("StudentModel", studentModel), 2)

                }
        })

        recyclerviewstudent?.adapter = studentadapter
    }
    fun viewclasslist(position : Int?){

        var findparticularstudent = studentdata.particularclassStudent(this,classId = position)

        var findparticularstudentadapter = StudentAdapter(findparticularstudent,object : StudentAdapter.StudentSelectedCallback{

            override fun onStudentClicked(studentModel: StudentModel) {

//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

                startActivityForResult(Intent(this@StudentActivity,UpdateDetail :: class.java).putExtra("StudentModel",studentModel),3)
            }
        })
        recyclerviewstudent?.adapter = findparticularstudentadapter
    }
}