package com.qi.student.Teacher

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

class TeacherModel() : Parcelable{

    var teacherID : Int = 0
    var teachername : String ?= ""
    var phonenumber : String ?= ""

    constructor(parcel: Parcel) : this() {
        teacherID = parcel.readInt()
        teachername = parcel.readString()
        phonenumber = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(teacherID)
        parcel.writeString(teachername)
        parcel.writeString(phonenumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TeacherModel> {
        override fun createFromParcel(parcel: Parcel): TeacherModel {
            return TeacherModel(parcel)
        }

        override fun newArray(size: Int): Array<TeacherModel?> {
            return arrayOfNulls(size)
        }
    }
}
//@Parcelize
//class TeacherModel (var teacherID : Int?, var teachername : String?, var phonenumber : String?) : Parcelable {
//
//    override fun toString(): String {
//        return super.toString()
//    }
//}