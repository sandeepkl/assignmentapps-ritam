package com.qi.student.Teacher

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.qi.student.R
import com.qi.student.Subject.InsertSubject
import kotlinx.android.synthetic.main.activity_update_teacher.*
import kotlinx.android.synthetic.main.update_student_data.*
import kotlinx.android.synthetic.main.update_student_data.updatephone
import kotlinx.android.synthetic.main.update_student_data.updateusername

class UpdateTeacherDetails : AppCompatActivity(){

    private var username : EditText ?= null
    private var phone : EditText ?= null
    private var updateteacher : Button?= null
    var teacherId : Int = 0
    var pos:Int?=0

    var subjectposition : Int ?= 0

    companion object{

        lateinit var insertSubject: InsertSubject

        lateinit var insertTeacher: InsertTeacher
    }
    init {

        insertSubject = InsertSubject(this)

        insertTeacher = InsertTeacher(this)
    }
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_update_teacher)

        var action = supportActionBar
        action?.setDisplayHomeAsUpEnabled(true)
        action?.setDisplayShowHomeEnabled(true)

        var subjectlist = insertSubject.fetchsubjectdetail(this)

        var spin = findViewById(R.id.spinner2) as Spinner

        var arrayadapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,subjectlist)

        arrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spin.adapter = arrayadapter

        spin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                pos=position

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }


        }
        if(intent.hasExtra("TeacherModel")){

            val teacherModel = intent.getParcelableExtra<TeacherModel>("TeacherModel")
            teacherId = teacherModel?.teacherID?:0
            show(teacherModel)
            updateusername.setText(teacherModel?.teachername)
            updatephone.setText(teacherModel?.phonenumber)
        }

        insertTeacher.open()


        username = findViewById(R.id.updateusername)
        phone = findViewById(R.id.updatephone)

        updateteacher = findViewById(R.id.updatedetail)

        updateteacher?.setOnClickListener {
            view()
            finish()
        }
//        Toast.makeText(this,"subject Index == $subjectposition",Toast.LENGTH_LONG).show()
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item.itemId == android.R.id.home){
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    fun view(){

        val usernameTostring = username?.text?.toString()
        val phoneToString = phone?.text?.toString()

        if(usernameTostring.equals("") || phoneToString.equals("")){
            Toast.makeText(this,"Please Fill Up",Toast.LENGTH_SHORT).show()
        }else{
            var status = insertTeacher.updateteacher(this,teacherId = teacherId,teachername = usernameTostring,teacherphone = phoneToString,subjectId = pos)
        }
    }
    fun show(teacherModel: TeacherModel){

        Toast.makeText(this,"teacher id === ${teacherModel.teachername}",Toast.LENGTH_LONG).show()
        username?.setText(teacherModel.teachername)
        phone?.setText(teacherModel.phonenumber)
    }
}